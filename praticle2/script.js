


function setAllImagesToGiphy() {
    const images = document.querySelectorAll('img');
    images.forEach((img) => {
      img.src = 'https://media.giphy.com/media/jaqvaWqpKfImQ/giphy.gif';
    });
  }
  function capitalizeAllParagraphs() {
    const paragraphs = document.querySelectorAll('p');
    paragraphs.forEach((p) => {
      p.textContent = p.textContent.toUpperCase();
    });
  }
  